import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  static Future<bool?> isLoggedin() async {
    final prefs = await SharedPreferences.getInstance();
    final isLoggedin = prefs.getBool('isLoggedin');
    return isLoggedin;
  }

  static Future<void> login() async {
    final prefs = await SharedPreferences.getInstance();
    final isLoggedin = prefs.setBool('isLoggedin', true);
  }

  static Future<void> logout() async {
    final prefs = await SharedPreferences.getInstance();
    final isLoggedin = prefs.setBool('isLoggedin', false);
  }

  static Future<void> setLang(String lang) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('lang', lang);
  }

  //get value from shared preferences
  static Future<String?> getLang() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String? lang = pref.getString('lang');
    return lang;
  }
}
