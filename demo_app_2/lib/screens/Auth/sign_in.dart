import 'package:demo_app_2/helpers/shared_prefs.dart';
import 'package:demo_app_2/helpers/validation.dart';
import 'package:demo_app_2/repositories/user_repo.dart';
import 'package:demo_app_2/screens/home.dart';
import 'package:demo_app_2/screens/Auth/sign_up.dart';
import 'package:demo_app_2/styles/colors.dart';
import 'package:demo_app_2/styles/constants.dart';
import 'package:demo_app_2/styles/widgets/circualr_black_button.dart';
import 'package:demo_app_2/styles/widgets/purple_button.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class SignIn extends StatelessWidget {
  SignIn({Key? key}) : super(key: key);
  final formKey = GlobalKey<FormState>();
  UserRepository userRepository = UserRepository();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'My App',
          style: TextStyle(fontFamily: 'Eva'),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            height: height(context),
            decoration: const BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage('assets/background.png'),
              ),
            ),
          ),
          Positioned(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    height: height(context) / 12,
                  ),
                  Container(
                    width: width(context) / 1.1,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(24)),
                      boxShadow: const [
                        BoxShadow(
                            color: Color(0x88020303),
                            offset: Offset(36, 8),
                            blurRadius: 64),
                        BoxShadow(
                            color: Color(0x10E8EDF3),
                            offset: Offset(-12, -20),
                            blurRadius: 54)
                      ],
                      border: Border.all(color: Colors.white12, width: 0.4),
                      gradient: const LinearGradient(
                          colors: [Color(0xff353A40), Color(0xff121416)],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          stops: [0.1, 0.6]),
                    ),
                    child: Center(
                        child: Form(
                      key: formKey,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                        child: Column(children: [
                          SizedBox(
                            height: height(context) / 14,
                          ),
                          Container(
                            decoration: textFieldContainerStyle(),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(8, 5, 8, 5),
                              child: TextFormField(
                                controller: emailController,
                                validator: (v) {
                                  if (v!.isValidEmail) {
                                    return null;
                                  } else {
                                    return "Please enter a Valid Email address";
                                  }
                                },
                                style: GoogleFonts.raleway(
                                    color: Color(0xffF0F0F0),
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 1.2),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Email',
                                  hintStyle: mainWhiteTextStyle(),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: height(context) / 17),
                          Container(
                            decoration: textFieldContainerStyle(),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                              child: TextFormField(
                                controller: passwordController,
                                validator: (v) {
                                  if (v!.isValidPassword) {
                                    return null;
                                  } else {
                                    return "Password should be at least 8 charcters and contains on uppercase letter";
                                  }
                                },
                                obscureText: true,
                                style: mainWhiteTextStyle(),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Password',
                                  hintStyle: GoogleFonts.raleway(
                                      color: Color(0xffF0F0F0),
                                      fontWeight: FontWeight.w400,
                                      letterSpacing: 1.2),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: height(context) / 17),
                          GestureDetector(
                            onTap: () async {
                              if (formKey.currentState!.validate()) {
                                try {
                                  dynamic result =
                                      await userRepository.signInUser(
                                    emailController.text,
                                    passwordController.text,
                                  );
                                } catch (e) {
                                  Fluttertoast.showToast(msg: e.toString());
                                }
                              }
                            },
                            child: GestureDetector(
                              onTap: () async {
                                {
                                  if (formKey.currentState!.validate()) {
                                    try {
                                      dynamic result =
                                          await userRepository.signInUser(
                                              emailController.text,
                                              passwordController.text);
                                      if (result != null) {
                                        Navigator.pushReplacement(
                                          context,
                                          PageTransition(
                                              child: HomePage(),
                                              type: PageTransitionType.fade),
                                        );
                                        SharedPrefs.login();
                                      }
                                    } catch (e) {
                                      Fluttertoast.showToast(
                                          msg: e.toString(),
                                          toastLength: Toast.LENGTH_LONG,
                                          textColor: Colors.white,
                                          backgroundColor: Colors.red[700]);
                                    }
                                  }
                                }
                              },
                              child: PurpleButton(action: 'Sign In'),
                            ),
                          ),
                          SizedBox(
                            height: height(context) / 28,
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.pushReplacement(
                                context,
                                PageTransition(
                                    child: SignUp(),
                                    type: PageTransitionType.fade),
                              );
                            },
                            child: RichText(
                              text: TextSpan(
                                style: GoogleFonts.epilogue(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                                children: [
                                  const TextSpan(
                                      text: 'Don\'t You have an Account? '),
                                  TextSpan(
                                    text: 'Sign Up',
                                    style: TextStyle(color: kPurpleLight),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: height(context) / 17),
                        ]),
                      ),
                    )),
                  ),
                  SizedBox(height: height(context) / 17),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CircularBlackIconButton(
                          icon: 'assets/google.png',
                        ),
                        CircularBlackIconButton(
                          icon: 'assets/facebook.png',
                        ),
                        CircularBlackIconButton(
                          icon: 'assets/linkedin.png',
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
