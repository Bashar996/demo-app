import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';

double height(context) => MediaQuery.of(context).size.height;
double width(context) => MediaQuery.of(context).size.width;

BoxDecoration textFieldContainerStyle() => BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(14)),
      boxShadow: const [
        BoxShadow(
          color: Color(0xff020303),
          offset: Offset(36, 8),
          blurRadius: 40,
        ),
        BoxShadow(
            color: Color(0x22E8EDF3), offset: Offset(-12, -20), blurRadius: 64)
      ],
      border: Border.all(color: Colors.white12, width: 0.4),
      gradient: const LinearGradient(
          colors: [
            Color(0xff33373A),
            Color(0xff1E2022),
          ],
          begin: Alignment.bottomRight,
          end: Alignment.topLeft,
          stops: [0.2, .8]),
    );

BoxDecoration circularBlackButtonStyle() => BoxDecoration(
      borderRadius: const BorderRadius.all(Radius.circular(40)),
      gradient: blackGrad(),
      boxShadow: const [
        BoxShadow(
            color: Color(0x88020303), offset: Offset(-12, -20), blurRadius: 64),
        BoxShadow(
            color: Color(0x10E8EDF3), offset: Offset(36, 8), blurRadius: 54)
      ],
    );

TextStyle mainWhiteTextStyle() => GoogleFonts.raleway(
    color: Color(0xffF0F0F0), fontWeight: FontWeight.w400, letterSpacing: 1.2);
