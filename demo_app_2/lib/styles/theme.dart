import 'package:demo_app_2/styles/colors.dart';
import 'package:flutter/material.dart';

ThemeData darkTheme = ThemeData(scaffoldBackgroundColor: kBlack);
