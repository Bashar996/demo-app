import 'package:flutter/material.dart';

import '../constants.dart';

class CircularBlackIconButton extends StatelessWidget {
  final String icon;
  CircularBlackIconButton({Key? key, required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(14),
      height: 60,
      width: 60,
      decoration: circularBlackButtonStyle(),
      child: Center(
        child: Image.asset(icon),
      ),
    );
  }
}
