import 'package:demo_app_2/styles/colors.dart';
import 'package:demo_app_2/styles/constants.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PurpleButton extends StatelessWidget {
  String action;
  PurpleButton({Key? key, required this.action}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height(context) / 18,
      width: width(context) / 1.8,
      decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
              color: Colors.white10, offset: Offset(-6, -6), blurRadius: 16.0),
          BoxShadow(
              color: Color(0x8838378A), offset: Offset(6, 9), blurRadius: 40)
        ],
        borderRadius: BorderRadius.all(Radius.circular(8)),
        gradient: LinearGradient(
          colors: [
            kPurpleLight,
            kPurpleDark,
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
      ),
      child: Center(
        child: Text(
          action,
          style: GoogleFonts.raleway(
              color: Colors.white,
              fontSize: width(context) / 20,
              fontWeight: FontWeight.w400),
        ),
      ),
    );
  }
}
