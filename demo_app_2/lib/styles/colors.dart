import 'package:flutter/material.dart';

Color kBlack = Color(0xff1D1F23);
Color kBlue = Color(0xff0579FF);
Color kPurpleLight = Color(0xff8D8BFF);
Color kPurpleDark = Color(0xff585EE6);

LinearGradient blackGrad() => const LinearGradient(
    colors: [Color(0xff353A40), Color(0xff121416)],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    stops: [0.1, 0.6]);
