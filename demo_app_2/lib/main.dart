import 'package:demo_app_2/helpers/shared_prefs.dart';
import 'package:demo_app_2/screens/Auth/sign_in.dart';
import 'package:demo_app_2/screens/home.dart';
import 'package:demo_app_2/styles/theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  SharedPrefs.isLoggedin().then(
    (value) => {
      runApp(
        MyApp(login: value),
      ),
    },
  );
}

class MyApp extends StatelessWidget {
  bool? login;

  MyApp({Key? key, required this.login}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: darkTheme,
        home: FutureBuilder(
            future: Firebase.initializeApp(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (login == true) {
                  return HomePage();
                } else {
                  return SignIn();
                }
              } else {
                return SignIn();
              }
            }));
  }
}
